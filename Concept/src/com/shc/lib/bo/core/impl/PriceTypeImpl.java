package com.shc.lib.bo.core.impl;

import com.shc.lib.bo.core.PriceType;

public class PriceTypeImpl implements PriceType {

	private String name;

	PriceTypeImpl(String name) {
		this.name = name;
	}

	@Override
	public String name() {
		return name;
	}

}
