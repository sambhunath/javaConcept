package org.sears.thread;

class CustomerThread extends Thread {
	CustomerThread() {

	}

	static Integer id = 0;
	static InheritableThreadLocal<Integer> threadLocal = new InheritableThreadLocal<Integer>() {
		/*
		 * @Override protected Integer initialValue() { return ++id; }
		 */

		public Integer childValue(Integer o) {
			return 999;
		}
	};

	CustomerThread(String name) {
		super(name);
	}

	public void run() {
		threadLocal.set(4);
		ChildCustomer ct = new ChildCustomer();
		ct.start();
		System.out.println("Current thread name " + Thread.currentThread().getName() + " threadLocal id " + threadLocal.get());

	}
}

class ChildCustomer extends Thread {
	public void run() {
		System.out.println("from Parent thread local set child thread local value  " + CustomerThread.threadLocal.get());
	}
}

public class ThreadLocalDemo {

	public static void main(String[] args) {
		CustomerThread t1 = new CustomerThread("Cust-1");
		CustomerThread t2 = new CustomerThread("Cust-2");
		CustomerThread t3 = new CustomerThread("Cust-3");
		CustomerThread t4 = new CustomerThread("Cust-4");
		t1.start();
		// t2.start();
		// t3.start();
		// t4.start();
	}

}
