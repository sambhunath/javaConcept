package org.sambhu;

import java.util.HashSet;
import java.util.Set;

public class PermutationTest {
	public String permutation(String str, int l, int r,Set<String> a) { //r = length of array,l=start index=0
		if (l == r) {
			//System.out.println(str);
			a.add(str);
		} else {
			for (int i = l; i <= r; i++) {
				str = swap(str, l, i);
				permutation(str, l + 1, r,a);
				str = swap(str, l, i);
			}
		}
		return str;
	}

	public String swap(String str, int i, int r) {
		char arr[] = str.toCharArray();
		char temp = arr[i];
		arr[i] = arr[r];
		arr[r] = temp;

		return String.valueOf(arr);
	}

	public static void main(String[] args) {
		String s = "abc";
		PermutationTest p = new PermutationTest();
		Set<String> a =new HashSet<String>(); 
		p.permutation(s, 0, s.length()-1,a); // string,0 index/start index,r=string length-1,a array list
		System.out.println("List of permutation combination String");
		a.forEach((e)-> {
                System.out.println(e);
        });
		//a.add("ddddds");
		System.out.println("");
		a.stream().filter(item -> (item.length() == 3)).forEach((e)->{
			System.out.println(e);
		});
		System.out.println(a.size());
		
	}

}
