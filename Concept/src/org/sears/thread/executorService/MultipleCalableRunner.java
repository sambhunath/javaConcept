package org.sears.thread.executorService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

class CallableTask<T> implements Callable<T> {
	T data;

	public CallableTask(T data1) {
		this.data = data1;
	}

	@Override
	public T call() throws Exception {
		Thread.sleep(1000);
		return data;
	}

}

public class MultipleCalableRunner {

	public static void main(String[] args) {
		ExecutorService executorService = Executors.newCachedThreadPool();

		
		@SuppressWarnings("rawtypes")
		List<CallableTask> tasks = new ArrayList<>();
		tasks.add(new CallableTask<String>("Hello"));
		tasks.add(new CallableTask<String>("Sambhu"));
		
		try {
			Future<String> re = executorService.submit(new CallableTask<String>("Hello"));
			@SuppressWarnings("unchecked")
			List<Future<String>> re1 = executorService.invokeAll((Collection<? extends CallableTask<String>>) tasks);
			System.out.println(re1.size());
			System.out.println(re.get());
			executorService.shutdown();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

	}

}
