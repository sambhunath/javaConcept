package com.shc.lib.bo.core;

public interface PriceType {
	String name(); // regular, clearance, promo, member
}
