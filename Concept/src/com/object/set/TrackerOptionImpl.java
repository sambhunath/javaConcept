package com.object.set;

import java.util.Collection;

public class TrackerOptionImpl implements TrackerOption{
	private Collection<String> tracker;

	public Collection<String> tracker() {
		return tracker;
	}

	public void setTracker(Collection<String> tracker) {
		this.tracker = tracker;
	}

}
