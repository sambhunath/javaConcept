package org.sambhu;


import java.io.IOException;
import java.net.InetSocketAddress;

import net.spy.memcached.MemcachedClient;

public class MemcachedTest {

	public static void main(String[] args) {
		// Connecting to Memcached server on localhost
	      MemcachedClient mcc = null;
		try {
			mcc = new MemcachedClient(new InetSocketAddress("127.0.0.1", 11211));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      System.out.println("Connection to server sucessfully");
	      
	      //not set data into memcached server
	      System.out.println("set status:"+mcc.set("tutorialspoint", 900, "memcached"));
	      
	      //Get value from cache
	      System.out.println("Get from Cache:"+mcc.get("tutorialspoint"));
	      
	      //Get available server
	      System.out.println("avl" +mcc.getAvailableServers() + " "+mcc.getThreadGroup());
	      
	}

}
