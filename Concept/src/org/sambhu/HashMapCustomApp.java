package org.sambhu;

class HashMapCustom<K, V> {
	private Entry<K, V>[] table; // Array of Entry.
	private int capacity = 4; // Initial capacity of HashMap

	static class Entry<K, V> {
		K key;
		V value;
		Entry<K, V> next;

		public Entry(K key, V value, Entry<K, V> next) {
			this.key = key;
			this.value = value;
			this.next = next;
		}
	}

	@SuppressWarnings("unchecked")
	HashMapCustom() {
		table = new Entry[capacity];
	}

	public int hash(K newKey) {
		return Math.abs(newKey.hashCode()) % capacity;
	}

	public void put(K newKey, V data) {

		if (newKey == null)
			return; // does not allow to store null.

		// calculate hash of key.
		int hash = hash(newKey);
		// create new entry.
		Entry<K, V> newEntry = new Entry<K, V>(newKey, data, null);

		// if table location does not contain any entry, store entry there.
		if (table[hash] == null) {
			table[hash] = newEntry;
		} else {
			Entry<K, V> previous = null;
			Entry<K, V> current = table[hash];

			while (current != null) { // we have reached last entry of bucket.
				if (current.key.equals(newKey)) {
					if (previous == null) { // node has to be insert on first of
											// bucket.
						newEntry.next = current.next;
						table[hash] = newEntry;
						return;
					} else {
						newEntry.next = current.next;
						previous.next = newEntry;
						return;
					}
				}
				previous = current;
				current = current.next;
			}
			previous.next = newEntry;
		}
	}

	public void display() {
		System.out.println();
	}

	/**
	 * 
	 * @param newKey
	 * @param data
	 */
	public void putData(K newKey, V data) {
		if (newKey == null)
			return;
		Entry<K, V> newEntry = new Entry<K, V>(newKey, data, null);
		// Index calculation of key using hashCode methode
		int hash = this.hashCodeCal(newKey);
		if (table[hash] == null) {
			table[hash] = newEntry;
		} else {
			Entry<K, V> previous = null;
			Entry<K, V> current = table[hash];
			while (current != null) {

				
			}
		}

	}

	/**
	 * 
	 * @param newKey
	 * @return
	 */
	public int hashCodeCal(K newKey) {
		if (newKey == null)
			return 0;
		return newKey.hashCode() % capacity;
	}

}

public class HashMapCustomApp {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		@SuppressWarnings("rawtypes")
		HashMapCustom hashMapCustom = new HashMapCustom();
		hashMapCustom.put(2, 12);
		hashMapCustom.put(25, 121);
		hashMapCustom.put(25, 131);
		hashMapCustom.put(5, 141);
		hashMapCustom.put(6, 171);
		hashMapCustom.put(25, 161);

		System.out.println("hashMapCustom");
		System.out.println("FF");

		hashMapCustom.put(4, 133);

	}

}
