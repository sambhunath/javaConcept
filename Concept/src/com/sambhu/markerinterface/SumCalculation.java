package com.sambhu.markerinterface;

public class SumCalculation implements Calculation {

	@Override
	public void calculate(Qualifiable qualifiable) {
		Double sum = null;
		if (qualifiable == null || !DoubleSupply.class.isAssignableFrom(qualifiable.getClass()))
			return;
		DoubleSupply doubleSupply = DoubleSupply.class.cast(qualifiable);
		sum = doubleSupply.a + doubleSupply.b;
		System.out.println("Sum " + sum);
	}

}
