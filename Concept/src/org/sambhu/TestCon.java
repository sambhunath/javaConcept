package org.sambhu;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TestCon {

	public static void main(String[] args) {
		TestCon t = new TestCon();
		try{
		t.start2();
		System.out.println("A");
		}catch(Exception e){
			System.out.println("B");
		}finally {
			System.out.println("C");
		}
		System.out.println("D");
		
		
		//Set with Array List
		Set<String> set = new HashSet<String>();
		set.add("1");
		set.add("2");
		set.add("3");
		set.add("4");
		set.add("1");
		set.add("4");
		System.out.println(set);
		
		List<String> list = new ArrayList<String>();
		list.add("1");
		//list.add("2");
		list.add("3");
		list.add("5");
		
		list.retainAll(set);
		
		set.retainAll(list);
		
		
		System.out.println(set.size() + " " + list.size());
		
		
		
	}

	/*void start(){
		long []  a1 = {3,4,5};
		long []  a2 = fix(a1);
		System.out.println(a1[0] +a1[1]+a1[2]+" ");
		System.out.println(a2[0] +a2[1]+a2[2]+" ");
	}
	
	long [] fix(long [] a3){
		a3[1]=7;
		return a3;
	}*/
	
	/*void start(){
		long a=7;
		long b = fix(a);
		System.out.println(a+" "+b);
	}
	
	long fix(long c){
		c=8;
		return c;
	}*/
	
	// Exception Step : 1
	public void start() throws Exception{
		throw new Exception();
	}
	
	// Exception Step : 2
		public void start2(){
			try {
				throw new Exception();
			} catch (Exception e) {
				System.out.println("S");
			}
		}
		// Exception Step : 2
	public void start3()
	{
		throw new Error();
	}	
}
