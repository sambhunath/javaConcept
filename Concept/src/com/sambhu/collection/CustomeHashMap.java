package com.sambhu.collection;

class HashMapCustom<K, V> {

	private Entry<K, V>[] table;
	private int capacity = 4;

	static class Entry<K, V> {
		K key;
		V value;
		Entry<K, V> next;

		public Entry(K newKey, V value, Entry<K, V> next) {
			this.key = newKey;
			this.value = value;
			this.next = next;
		}
	}

	@SuppressWarnings("unchecked")
	public HashMapCustom() {
		table = new Entry[capacity];
	}

	private int hash(K key) {
		return Math.abs(key.hashCode() % capacity);
	}

	/**
	 * 
	 * @param key
	 */
	private void validate(K key) {
		if (key == null) {
			String errMsg = "Key is required argument to ";
			throw new IllegalArgumentException(errMsg);
		}
	}

	/**
	 * This method put key value in custom hash.
	 * 
	 * @param newKey
	 * @param value
	 */
	public void putData(K newKey, V value) {
		validate(newKey);
		// calculated hash or bucket index
		int hash = hash(newKey);
		// New entry create
		Entry<K, V> newEntry = new Entry<K, V>(newKey, value, null);

		if (table[hash] == null) {
			table[hash] = newEntry;
			return;
		} else {
			Entry<K, V> preview = null;
			Entry<K, V> current = table[hash];
			while (current != null) {
				if (current.key.equals(newKey)) {
					if (preview == null) {
						newEntry.next = current.next;
						table[hash] = newEntry;
						return;
					} else {
						newEntry.next = current.next;
						preview.next = newEntry;
						return;
					}

				}
				preview = current;
				current = current.next;
			}
			preview.next = newEntry;
		}
	}

	/**
	 * To display custom hash map
	 */
	public void display() {
		for (int i = 0; i < capacity; i++) {
			Entry<K, V> entry = table[i];
			while (entry != null) {
				System.out.println(entry.key + " " + entry.value);
				entry = entry.next;
			}
		}
	}
}

public class CustomeHashMap {

	public static void main(String[] args) {
		HashMapCustom<Integer, Integer> hashMapCustom = new HashMapCustom<Integer, Integer>();
		hashMapCustom.putData(21, 12);
		hashMapCustom.putData(25, 121);
		hashMapCustom.putData(30, 151);
		hashMapCustom.putData(33, 15);
		hashMapCustom.putData(35, 89);

		hashMapCustom.display();
	}

}
