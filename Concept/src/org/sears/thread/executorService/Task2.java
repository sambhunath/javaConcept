package org.sears.thread.executorService;

public class Task2 implements Runnable {

	@Override
	public void run() {
		System.out.println("Task2--> ");
		for (int i = 20; i > 0; i++) {
			System.out.print(i +" ");
		}
		System.out.println("Task2 is done");
	}

}
