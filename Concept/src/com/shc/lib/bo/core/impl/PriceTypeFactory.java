package com.shc.lib.bo.core.impl;

import java.util.HashMap;
import java.util.Map;

import com.shc.lib.bo.core.PriceType;

public class PriceTypeFactory {

	private static final Map<String, PriceType> priceTypes = new HashMap<String, PriceType>();

	public static final PriceType getPriceType(String priceTypeName) {
		PriceType priceType = priceTypes.get(priceTypeName);
		if (priceType != null) {
			return priceType;
		}
		synchronized (PriceTypeFactory.class) {
			priceType = priceTypes.get(priceTypeName);
			if (priceType == null) {
				priceType = new PriceTypeImpl(priceTypeName);
				priceTypes.put(priceTypeName, priceType);
			}
			return priceType;
		}
	}

}
