package org.sears.thread.executorService;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("hiding")
public class FactorialCalculation<Integer> implements Callable<Integer> {

	private int number;

	FactorialCalculation(int num) {
		this.number = num;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Integer call() throws Exception {
		 int result = 1;
	        if ((number == 0) || (number == 1)) {
	            result = 1;
	        } else {
	            for (int i = 2; i <= number; i++) {
	                result *= i;
	                TimeUnit.MILLISECONDS.sleep(20);
	            }
	        }
	        System.out.println("Result for number - " + number + " -> " + result);
	        return (Integer) (new java.lang.Integer(result));
	}

}
