package org.sambhu;

import java.util.LinkedList;
import java.util.Queue;

public class TwoQueueStack {

	Queue<Integer> queue1 = new LinkedList<Integer>();
	Queue<Integer> queue2 = new LinkedList<Integer>();

	// Push element in stack using Queue.
	public void push(int itemElemnt) {
		if (!queue1.isEmpty()) {
			for (int i = 0; i < queue1.size(); i++) {
				int itemElem = queue1.poll();
				queue2.add(itemElem);
			}
		}
		queue1.add(itemElemnt);
		if (!queue2.isEmpty()) {
			for (int i = 0; i < queue2.size(); i++) {
				int itemElem = queue2.poll();
				queue1.add(itemElem);
			}
		}
	}

	// POP element in stack using Queue.
	public void pop() {
		if (!queue1.isEmpty()) {
			int popElem = queue1.poll();
			System.out.println("POP element is : " + popElem);

		}
	}

	public static void main(String[] args) {

	}

}
