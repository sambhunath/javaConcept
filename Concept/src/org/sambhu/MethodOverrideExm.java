package org.sambhu;

class ParentClass {
	protected void disply() throws Exception {
		System.out.println("Parent");
	}
}

public class MethodOverrideExm extends ParentClass {
	@Override
	public void disply() throws Exception {
		super.disply();
		System.out.println("child");
	}

	public static void main(String[] args) throws Exception {
		ParentClass parentClass = new MethodOverrideExm();
		parentClass.disply();
	}

}
