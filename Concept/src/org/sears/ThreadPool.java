package org.sears;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPool {
	
	static ExecutorService executorService;
	
	static{
		executorService = Executors.newCachedThreadPool();
	}

	public static void main(String[] args) {
		 String qualifiedHostName = System.getProperty("jboss.qualified.host.name"); 
		 
         String providedHostName = System.getProperty("jboss.host.name");
         
         System.out.println("qualifiedHostName"+qualifiedHostName);
         System.out.println("providedHostName"+providedHostName);
	}

}
