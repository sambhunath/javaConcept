package org.sears.thread.executorService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class TaskExecutorService {
	public static void main(String[] args) {
      ExecutorService executorService =  Executors.newSingleThreadExecutor();
      executorService.submit(new Task1());
      System.out.println("");
      executorService.submit(new Thread(new Task2()));
      
      executorService.shutdown();
	}

}
