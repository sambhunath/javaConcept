package com.sambhu.markerinterface;

public class SubCalculation implements Calculation {
	@Override
	public void calculate(Qualifiable qualifiable) {
		Integer result = null;
		if (qualifiable == null || !IntegerSupply.class.isAssignableFrom(qualifiable.getClass()))
			return;
		IntegerSupply integerSupply = IntegerSupply.class.cast(qualifiable);
		if (integerSupply.a > integerSupply.b)
			result = integerSupply.a - integerSupply.b;
		else
			result = integerSupply.b - integerSupply.a;
		System.out.println("Result of Substraction : " + result);
	}

}
