package com.sambhu;

import java.util.Comparator;

class Epm {
	int id;
	int age;

	Epm(int id, int age) {
		this.id = id;
		this.age = age;
	}
}

class EmpCom implements Comparator<Epm> {
	@Override
	public int compare(Epm o1, Epm o2) {
		int flag = 0;
		flag = o1.id - o2.id;
		if (flag == 0) {
			return o1.age - o2.age;
		}
		return 0;
	}
}

public class MainTest {

	public static void main(String[] args) {
		System.out.println("String");
	}

	public static void main(int[] args) {
		System.out.println("int");
	}

	public float main(float[] args) {
		System.out.println("int");
		return 0;
	}

	Epm e = new Epm(1, 4);
	Epm e1 = new Epm(1, 3);

}
