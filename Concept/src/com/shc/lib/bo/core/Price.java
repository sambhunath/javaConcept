package com.shc.lib.bo.core;

public interface Price {

	PriceType priceType();

	Double value();

}
