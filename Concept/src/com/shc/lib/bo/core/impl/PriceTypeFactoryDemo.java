package com.shc.lib.bo.core.impl;

import com.shc.lib.bo.core.PriceType;

public class PriceTypeFactoryDemo {

	public static void main(String[] args) {
		PriceTypeFactory priceTypeFactory = new PriceTypeFactory();
		@SuppressWarnings("static-access")
		PriceType priceType = priceTypeFactory.getPriceType("selling");
		System.out.println(priceType.getClass());

	}

}
