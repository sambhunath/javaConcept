package org.sears;

import org.sambhu.ProtectrdDataType;

public class AccessSpecifierDiffPkg extends ProtectrdDataType {

	public static void main(String[] args) {
		AccessSpecifierDiffPkg acpkg = new AccessSpecifierDiffPkg();
		int temp = acpkg.i = 7;
		System.out.println("Sub class in different pkg : " + temp);
		acpkg.display();
	}
}
