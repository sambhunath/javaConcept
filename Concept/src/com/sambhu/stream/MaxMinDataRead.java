package com.sambhu.stream;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Comparator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

public class MaxMinDataRead {

	public static void main(String[] args) {
		Set<Integer> numbers = new TreeSet<Integer>();
		numbers.add(1);
		numbers.add(3);
		numbers.add(2);
		numbers.add(5);

		String min = Stream.of(numbers.toString()).min(Comparator.comparing(Integer::valueOf)).get();
		System.out.println(min);

		Integer max = numbers.stream().reduce(Integer.MAX_VALUE, ((x, y) -> Integer.min(x, y)));
		System.out.println("Max " + max);

		Integer maxNumber = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9).max(Comparator.comparing(Integer::valueOf)).get();
		System.out.println(maxNumber);
		
		System.out.println("Show even & odd using lamda java 8");
		numbers.forEach(n-> {
			if(n%2==0)
			System.out.println(n);
		});
		
		System.out.println("From File ");
		Scanner scanner = null;
		
		try {
			scanner = new Scanner(new File("C:\\Users\\ssaman1\\Documents\\Sambhunath\\testFile.txt"));
			numbers = new TreeSet<Integer>();
			while (scanner.hasNext()) {
				numbers.add(Integer.parseInt(scanner.next()));
			}
			
			Integer maxData = numbers.stream().reduce(Integer.MAX_VALUE, ((x,y)->Integer.max(x,y)));
			System.out.println("Max data "+maxData);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			scanner.close();
		}
		
		
	}

}
