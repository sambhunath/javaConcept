package org.sambhu;

public class StarProblem {

	public static void main(String[] args) {
		int n=5;
		printStars(n);
	}

	private static void printStars(int n) {
		// TODO Auto-generated method stub
		
		for(int i=0;i<n;i++){
			for(int j=0;j<=i;j++){
				System.out.print(" * ");
			}
			System.out.println(" ");
		}
		System.out.println("..................");
		for(int i=n;i>=1;--i){
			for(int j=1;j<=i;++j){
				System.out.print("* ");
			}
			System.out.println(" ");
		}
		System.out.println("..................");
		int m=7,count=m-1;
		for(int k=1;k<=m;k++){
			for(int sp=1;sp<=count;sp++){
				System.out.print(" ");
			}
			count--;
			for(int j=1;j<=2*k-1;j++){
				System.out.print("* ");
			}
			System.out.println();
		}
	}

}
