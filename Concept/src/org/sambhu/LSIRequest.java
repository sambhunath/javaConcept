package org.sambhu;

public class LSIRequest {

	public static void main(String[] args) {

		int arr[] = { 9,1,3,7,5,6,20 };
		int max[] = new int[arr.length];
		int result=1;
		for (int i = 0; i < arr.length; i++) {
			max[i] = 1;
			for (int j = 0; j < i; j++) {
				if (arr[i] > arr[j]) {
					max[i] = Math.max(max[i], max[j]+1);
				}
			}
			result = Math.max(max[i], result);
			
		}
		printList(max,max.length-1,arr,result);
		
	}
	
	public static void printList(int []lis,int lastIndex,int []arr,int max){
		if(max==0){
			return;
		}
		if(lis[lastIndex]==max){
			printList(lis,lastIndex-1,arr,max-1);
			System.out.print(arr[lastIndex] + " ");
		}else{
			printList(lis,lastIndex-1,arr,max);
		}
	}
	

}
