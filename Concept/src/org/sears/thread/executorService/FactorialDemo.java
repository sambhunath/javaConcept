package org.sears.thread.executorService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FactorialDemo {
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		ExecutorService executorService = Executors.newCachedThreadPool();
		List<Future<Integer>> results=new ArrayList<>();
		List<Future<FactorialCalculation>> tasks=new ArrayList<Future<FactorialCalculation>>();
		for(int i=1;i<10;i++){
			tasks.add((Future<FactorialCalculation>) new FactorialCalculation<Integer>(i));
			
			Future<Integer> result = executorService.submit(new FactorialCalculation<Integer>(i));
			
			System.out.println(result.get());
			results.add(result);
		}
		Future<FactorialCalculation> result1 = executorService.invokeAll()(tasks);
		
		executorService.shutdown();

	}
}
