package org.sambhu;

public final class CustomImmutable {

	private final int i;
	
	public int getI() {
		return i;
	}

	public CustomImmutable(int x){
		this.i=x;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CustomImmutable ci = new CustomImmutable(10);
		//System.out.println(ci.getI());
		//ci.setI(20);
		//ci.i=20;
		 ci= new CustomImmutable(20);
		System.out.println(ci.i);
	}

	

}
