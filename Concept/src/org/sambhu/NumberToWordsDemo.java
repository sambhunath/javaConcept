package org.sambhu;

public class NumberToWordsDemo {
	private final static String unitsMap[] = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight",
			"nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen",
			"nineteen" };

	private final static String tensMap[] = { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy",
			"eighty", "ninety" }; 

	public static String numberToWord(int number) {
		if (number == 0)
			return "zero";
		if (number < 0)
			return "minus" + numberToWord(Math.abs(number));
		String words = " ";
		if ((number / 1000000000) > 0) {
			words += numberToWord(number / 1000000000) + " bilion";
			number %= 1000000000; 
		}
		if ((number / 1000000) > 0) {
			words += numberToWord(number / 1000000) + " milion";
			number %= 1000000;
		}
		if ((number / 1000) > 0) {
			words += numberToWord(number / 1000) + " thousend";
			number %= 1000;
		}
		if ((number / 100) > 0) {
			words += numberToWord(number / 100) + " hundred";
			number %= 100;
		}

		if (number > 0) {
			if (number < 20) 
				words +=" "+ unitsMap[number] ;
			 else {
				words += " "+tensMap[number / 10];
				if ((number % 10) > 0)
					words += " " + unitsMap[number % 10];
			}

		}
		return words;
	}

	public static void main(String[] args) {
		System.out.println(numberToWord(99999));
	}

}
