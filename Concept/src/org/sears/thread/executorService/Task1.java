package org.sears.thread.executorService;

public class Task1 extends Thread {
	@Override
	public void run() {
		System.out.println("Task1--> ");
		for (int i = 1; i < 10; i++) {
			System.out.print(i + " ");
		}
		System.out.println("Task1 is done");
	}
}
