package com.sambhu.ll;

public class LinkedListDemo {

	Node node;

	static class Node {
		Node next;
		int ele;

		Node(int data) {
			this.ele = data;
			next = null;
		}
	}

	/* This function prints contents of linked list starting from head */
	public void printList() {
		Node n = node;
		int tem = 0;
		while (n != null) {
			System.out.print(n.ele + " ");
			if (n.ele == 1) {
				tem = n.ele;
				n.ele = n.next.ele;
				n.next.ele = tem;
			}
			n = n.next;

		}
	}

	public static void main(String[] args) {
		LinkedListDemo ld = new LinkedListDemo();
		ld.node = new Node(1);
		Node second = new Node(2);
		Node third = new Node(3);

		ld.node.next = second;
		ld.node.next.next = third;

		ld.printList();
	}
}
