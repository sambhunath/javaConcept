package com.object.set;

import java.util.Collection;

public interface TrackerOption {
	Collection<String> tracker();
}
