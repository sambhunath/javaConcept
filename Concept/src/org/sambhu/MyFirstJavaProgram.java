package org.sambhu;

class A {
	private int a() {
		return 12;
	}

}

class B extends A {
	void callA() {
		try {
			java.lang.reflect.Method m = getClass().getSuperclass().getDeclaredMethod("a", new Class<?>[] {});
			m.setAccessible(true);
			System.out.println(m.isAccessible());
			Object i = m.invoke(this, (Object[]) null);
			System.out.println(i.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

public class MyFirstJavaProgram {

	/*
	 * This is my first java program. This will print 'Hello World' as the
	 * output
	 */

	public static void main(String[] args) {
		B b = new B();
		b.callA();
		System.out.println("Hello World"); // prints Hello World
	}
}