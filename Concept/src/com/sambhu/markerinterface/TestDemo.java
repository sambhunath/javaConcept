package com.sambhu.markerinterface;

public class TestDemo {

	static Calculation calculationSum = new SumCalculation();
	static Calculation calculationSub = new SubCalculation();

	public static void main(String[] args) {
		IntegerSupply integerSupply = new IntegerSupply();
		calculationSub.calculate(integerSupply);

		DoubleSupply doubleSupply = new DoubleSupply();
		calculationSum.calculate(doubleSupply);

	}

}
