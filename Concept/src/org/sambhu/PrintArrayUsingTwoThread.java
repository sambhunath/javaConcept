package org.sambhu;

import java.util.concurrent.CountDownLatch;

public class PrintArrayUsingTwoThread {

	static int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	volatile static int i = 0;

	public static void main(String[] args) {

		CountDownLatch countDownLatch = new CountDownLatch(2);

		Thread threadEven = new Thread(new Runnable() {
			public void run() {
				synchronized (arr) {
					for (int k = 0; k < arr.length; k++) {
						if (k == arr.length - 1) {
							return;
						}
						try {
							System.out.println(Thread.currentThread().getName() + " odd : " + arr[i++]);
							if (i % 2 != 0) {
								arr.notify();
								arr.wait();
							}
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		});

		Thread threadOdd = new Thread(new Runnable() {
			public void run() {

				synchronized (arr) {
					for (int l = 0; l < arr.length; l++) {
						if (l == arr.length - 1) {
							return;
						}
						try {
							System.out.println(Thread.currentThread().getName() + " even : " + arr[i++]);
							if (i % 2 == 0) {

								arr.notify();
								arr.wait();
							}
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		});
		try {
			threadOdd.start();
			threadEven.start();
			threadOdd.join();
			threadEven.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
