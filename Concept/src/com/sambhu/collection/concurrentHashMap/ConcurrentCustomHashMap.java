package com.sambhu.collection.concurrentHashMap;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class ConcurrentCustomHashMap<K, V> {
	private static final int LOCK_TIMEOUT_MILLISECONDS = 5000;
	private static final String LOCK_TIMEOUT_MSG = "Caught InterruptedException while trying to acuire lock, time out: "
			+ LOCK_TIMEOUT_MILLISECONDS + " ms";

	private Entry<K, V> table;
	private int capacity = 4;

	static class Entry<K, V> {
		K key;
		V value;
		Entry<K, V> next;

		public Entry(K newKey, V value, Entry<K, V> next) {
			this.key = newKey;
			this.value = value;
			this.next = next;
		}
	}

	public void put(K newKey, V value) {
		validate(newKey);
		ReentrantLock lock = getLock(newKey);
		try {
			if (lock.tryLock(LOCK_TIMEOUT_MILLISECONDS, TimeUnit.MILLISECONDS)) {
				try {

				} finally {
					lock.unlock();
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get the ReentrantLock lock for a specific key.
	 * 
	 * @param key
	 * @return
	 */
	private ReentrantLock getLock(K key) {
		validate(key);
		FlowContextData<K, ReentrantLock> flowContext = flowContextData.get();
		ReentrantLock lock = flowContext.shareLock.get(key);
		if (lock != null) {
			return lock;
		}
		synchronized (flowContext.shareLock) {
			lock = flowContext.shareLock.get(key);
			if (lock == null) {
				lock = new ReentrantLock();
				flowContext.shareLock.put(key, lock);
			}
		}
		return lock;

	}

	/**
	 * 
	 * @param key
	 */
	private void validate(K key) {
		if (key == null) {
			String errMsg = "Key is required argument to ";
			throw new IllegalArgumentException(errMsg);
		}
	}

	// Thread local for shared lock
	ThreadLocal<FlowContextData<K, ReentrantLock>> flowContextData = new ThreadLocal<FlowContextData<K, ReentrantLock>>() {
		@Override
		protected FlowContextData<K, ReentrantLock> initialValue() {
			FlowContextData<K, ReentrantLock> flowContextData = new FlowContextData<>();
			flowContextData.shareLock = new HashMap<K, ReentrantLock>();
			return flowContextData;
		}
	};

}
