package org.sambhu;

public class OddWordPrint {

	public static void main(String[] args) {
		String in = "12345";
		for (int i = 0; i < in.length(); i++) {
			for (int k = 0; k < in.length(); k++) {
				if (i == k || k==in.length()-1-i) {
					System.out.print(in.charAt(k)+" ");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println(" ");
		}
	}

}
