package org.sambhu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Emp {
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private String name;

	Emp(int id, String name) {
		this.id = id;
		this.name = name;
	}
}

public class LamdaAnno {

	public static void main(String[] args) {

		Emp emp = new Emp(1, "Sears");
		Emp emp1 = new Emp(2, "Kmart");

		ArrayList<Emp> list = new ArrayList<Emp>();
		list.add(emp1);
		list.add(emp);

		final int s = 0;
		Runnable r = new Runnable() {
			@Override
			public void run() {
				int s = 10;
				System.out.println("hello anonymous  " + s);

			}
		};

		Thread thread = new Thread(r);
		thread.start();

		Runnable runnable = () -> {
			int a = 20;
			System.out.println("Hi lamda " + a);
		};

		Thread thread1 = new Thread(runnable);
		thread1.start();

		System.out.println("---" + s);

		Collections.sort(list, new Comparator<Emp>() {
			@Override
			public int compare(Emp emp, Emp em) {
				return emp.getName().compareTo(emp.getName());
			}
		});

		Collections.sort(list, (Emp e, Emp em) -> e.getName().compareTo(em.getName()));

		System.out.println(list);

		ArrayList<Integer> list1 = new ArrayList<Integer>();
		list1.add(0, 8);
		list1.add(1, 1);
		list1.add(2, 4);
		list1.add(0, 2);
		list1.add(1, 13);
		
		System.out.println(list1);
	}

}
