package com.sambhu.string;

import java.util.Set;
import java.util.TreeSet;

public class PermutationOfString {

	private static Set<String> permute(String input, int l, int r,Set<String> pcString) {
		if (l == r) {
			pcString.add(input);
		} else {
			for (int i = l; i < r; i++) {
				input = swap(input, l, i);
				permute(input, l + 1, r,pcString);
				input = swap(input, l, i);
			}

		}
		return pcString;
	}

	private static String swap(String input, int l, int r) {
		char temp;
		char[] charArray = input.toCharArray();
		temp = charArray[l];
		charArray[l] = charArray[r];
		charArray[r] = temp;
		return String.valueOf(charArray);
	}

	public static void main(String[] args) {
		Set<String> pcString = new TreeSet<String>();
		pcString = permute("abcd", 0, "abcd".length(),pcString);
		System.out.println(pcString.size());
	}

}
