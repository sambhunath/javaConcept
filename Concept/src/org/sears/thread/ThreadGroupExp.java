package org.sears.thread;

import java.util.List;

class MyThread extends Thread {
	public MyThread(ThreadGroup g, String name) {
		super(g, name);
	}

	@Override
	public void run() {
     System.out.println("Child thread");
     try{
    	 Thread.sleep(5000);
     }catch(InterruptedException e){
    	e.printStackTrace(); 
     }
	}
}

public class ThreadGroupExp {
	public static void main(String[] args) {
		ThreadGroup pg = new ThreadGroup("Parent group");
		ThreadGroup cg = new ThreadGroup(pg, "Child grp");
		

		System.out.println("getMaxPriority-- > " + pg.getMaxPriority());
		System.out.println("getParent().getName()-- > " + pg.getParent().getName());

		Thread t1 = new Thread(pg, "1st");
		Thread t2 = new Thread(pg, "2nd");
		//t1.setPriority(5);
		//t2.setPriority(5);
		t1.start();
		t2.start();
		System.out.println("Active count: "+pg.activeCount());
		System.out.println("Active grp: "+pg.activeGroupCount());
		System.out.println("List of thread:");
		pg.list();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("After sleep");
		
		System.out.println("Active count: "+pg.activeCount());
		System.out.println("Active grp "+pg.activeGroupCount());
		System.out.println("List of thread: ");
		pg.list();
		

		System.out.println("t1/t2.getPriority()-- > " + t1.getPriority());
		pg.setMaxPriority(3);

		Thread t3 = new Thread(pg, "3rd");

		System.out.println("t3.getPriority()--> " + t3.getPriority());

		Thread[] t = new Thread[3];
		pg.enumerate(t);

		for (Thread thread : t) {
			System.out.println(thread.getName());
		}

		

		System.out.println("g2.getParent().getName()--> " + cg.getParent().getName());

	}
}
