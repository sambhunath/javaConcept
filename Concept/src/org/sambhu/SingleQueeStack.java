package org.sambhu;

import java.util.LinkedList;
import java.util.Queue;

public class SingleQueeStack {
	Queue<Integer> queue = new LinkedList<Integer>();

	public void push(int num) {
		queue.add(num);
		// pop all font element and put in rear into queue.
		for (int i = 0; i < queue.size()-1; i++) {
			int itemElement = queue.poll();
			queue.add(itemElement);
		}
	}

	public void pop() {
		if (queue.isEmpty()) {
			System.out.println("Queue is empty");
			return;
		}
		int itemEle = queue.poll();
		System.out.println("POP element is" + itemEle);
	}

	public static void main(String[] args) {
		SingleQueeStack singleQueeStack = new SingleQueeStack();
		singleQueeStack.push(1);
		singleQueeStack.push(2);
		singleQueeStack.push(3);
		
		singleQueeStack.pop();
		
		singleQueeStack.push(4);
		singleQueeStack.push(5);
		
		singleQueeStack.pop();
	}

}
