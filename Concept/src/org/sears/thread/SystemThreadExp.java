package org.sears.thread;

public class SystemThreadExp {

	public static void main(String[] args) {
		new Thread();
		ThreadGroup system = Thread.currentThread().getThreadGroup().getParent();
		Thread [] t = new Thread[system.activeCount()];
		system.enumerate(t);
		for(Thread thread:t){
			System.out.println("System thread name: "+thread.getName()+" is demon " +thread.isDaemon());
		}

	}

}
